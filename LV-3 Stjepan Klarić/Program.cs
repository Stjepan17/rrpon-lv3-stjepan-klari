﻿using System;

namespace LV_3_Stjepan_Klarić
{
    class Program
    {
        static void Main(string[] args)
        {
            //ZADATAK 1 - Ovisno o tome što nas traži, ako nam treba dva različita objekta koji imaju iste atribute, ali su neovisno jedan o drugom
            //onda koritimo duboko kloniranje, koje "duplicira" objekt, dok kod plitkog kloniranja objekti dijele iste atribute i ovisni su jedan o drugom.

            //Dataset obj1 = new Dataset("Abs.csv");
            //Dataset obj2 = (Dataset)obj1.Clone();
            //obj1.PrintData();
            //Console.WriteLine();
            //obj2.PrintData();


            //ZADATAK 2
            //RandomGenerator obj = RandomGenerator.GetInstance();
            //obj.postavi(3, 3);
            //obj.popuniMatrix();
            //obj.printMatrix();


            //ZADATAK 3
            //Logger logger = Logger.GetInstance();
            //logger.Log("Asdfg");

            //ZADATAK 4
            //ConsoleNotification Log = new ConsoleNotification("Stjepan Klarić", "Crvena", "Mračna crvena", DateTime.Now, Category.ERROR, ConsoleColor.DarkYellow);
            //NotificationManager notification = new NotificationManager();
            //notification.Display(Log);


        }
    }
}

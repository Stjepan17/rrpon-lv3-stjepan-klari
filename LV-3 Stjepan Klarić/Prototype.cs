﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Stjepan_Klarić
{
    interface Prototype
    {
        Prototype Clone();
    }
}

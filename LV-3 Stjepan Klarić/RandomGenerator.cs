﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Stjepan_Klarić
{
    class RandomGenerator
    {
        private static RandomGenerator instance;
        private Random generator = new Random();
        private double[,] matrix;
        private int m, n;
        public void postavi(int a, int b)
        {
            m = a;
            n = b;
            matrix = new double[m, n]; //proširivanje matrice
        }

        private RandomGenerator()
        {
            matrix = new double[1, 1]; //kreiranje matrice 1x1 koju ćemo proširiti pri pozivu metode postavi
        }

        public static RandomGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomGenerator();
            }
            return instance;
        }


        public void popuniMatrix()
        {
            int i, j;
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; j++)
                {
                    matrix[i, j] = generator.NextDouble();
                }
            }
        }


        public void printMatrix()
        {
            int i, j;
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Stjepan_Klarić
{
    class Logger
    {
        private String filePath;
        private static Logger instance;

        private Logger()
        {
            this.filePath = "zad.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public String Filepath
        {
            set { filePath = value; }
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter fileWriter =
            new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(message);
            }
        }
    }
}

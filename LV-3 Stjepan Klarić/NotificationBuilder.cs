﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Stjepan_Klarić
{
    class NotificationBuilder : IBuilder
    {
        private String author;
        private String title;
        private DateTime time;
        private Category level;
        private ConsoleColor color;
        private String text;

        public ConsoleNotification Build()
        {
            return new ConsoleNotification(author, title, text, time, level, color);
        }

        public IBuilder SetAuthor(string author) {
            this.author = author;
            return this;
        
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            this.level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.time = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.title = title;
            return this;
        }
    }
}
